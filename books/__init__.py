from flask import Flask

def create_app():
	app = Flask(__name__, static_url_path='/static')

	#import books.views
	from books.views import books
	app.register_blueprint(books)
	
	return app
