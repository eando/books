from flask import request, redirect, url_for, render_template, flash, session
from flask import current_app as app
from books import shelfAPI
from books import get_bookinfo
from flask import Blueprint
from werkzeug.exceptions import HTTPException
from books.yurindo import zaiko, zaiko_all

books = Blueprint('books', __name__)

@books.errorhandler(HTTPException)
def handle_exception(e):
    response = "エラー (error code:" + str(e.code) + ")"
    return render_template('books/result.html', result = response)

@books.route('/')
def show_top():
	return render_template('books/index.html')

@books.route('/tsundoku')
def input_userid():
    return render_template('books/tsundoku.html')

@books.route('/search', methods=['GET','POST'])
def search_tsundoku():
    error = None
    if request.method == 'POST':
        resultBooklog = shelfAPI.get_tsundoku(request.form['userid'], request.form['categoryid'])
        return render_template('books/result_list.html', result = resultBooklog)
    else:
        return "NOT POST METHOD"

@books.route('/amazon-info', methods=['GET','POST'])
def input_amazoninfo():
    return render_template('books/input_amazon_info.html')

@books.route('/amazon-info/info', methods=['POST'])
def get_amazon_info():
    error = None
    if request.method == 'POST':
        inputdata1 = request.form['amazon_info1']
        inputdata2 = request.form['amazon_info2']
        result = get_bookinfo.extract_amazoninfo(inputdata1, inputdata2)
    else:
        return "NOT POST METHOD"
    return render_template('books/result_amazon.html', result = result)

@books.route('/isbn', methods=['GET','POST'])
def input_isbn():
    return render_template('books/input_isbn.html')

@books.route('/quote', methods=['GET','POST'])
def get_quote():
    error = None
    if request.method == 'POST':
        resultGoogleAPI = get_bookinfo.book_quote(request.form['isbn'])
        return render_template('books/result_map.html', result = resultGoogleAPI)
    else:
        return "NOT POST METHOD"

@books.route('/yurindo', methods=['GET','POST'])
def yurindo():
    return render_template('books/input_yurindo.html')

@books.route('/zaiko', methods=['GET','POST'])
def zaiko_kensaku():
    error = None
    if request.method == 'POST':
        tenpo_list = ['0520','0510','0420']
        tenpo_name_list = ['アトレ大井町','グランデュオ蒲田','アトレ川崎']
        if request.form['tenpo'] != '':
            tenpo_list.append(request.form['tenpo'])
            tenpo_name_list.append("###")

        # 書籍情報を取得（Google Books API）
        resultGoogleAPI = get_bookinfo.book_quote(request.form['isbn'])
        # zaiko_list:在庫状況リスト, tana_list:棚番号リスト
        zaiko_list,tana_list = zaiko_all(request.form['isbn'], tenpo_list)
        return render_template('books/result_zaiko.html', tenpo_list = tenpo_list, tenpo_name_list = tenpo_name_list, zaiko_list = zaiko_list, tana_list = tana_list, result = resultGoogleAPI)
    else:
        return "NOT POST METHOD"
