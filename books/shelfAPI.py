import requests
import json
import configparser
import random

def get_tsundoku(userid,categoryid):
    # 設定ファイル読み込み
    config_ini = configparser.ConfigParser()
    config_ini.read('books/config.ini', encoding='utf-8')

    # 設定ファイルの値を変数に設定
    user = userid
    category = categoryid
    apibasepath = config_ini['GENERAL']['ApiBasePath']
    queryparam = config_ini['GENERAL']['QueryParam']

    # APIとして呼び出すURL
    url = apibasepath + user + queryparam + '&category=' + category

    #requests.getを使うと、レスポンス内容を取得できるのでとりあえず変数へ保存
    response = requests.get(url)

    #response.json()でJSONデータに変換して変数へ保存
    jsonData = response.json()

    num = 0
    booklist = []
    for jsonObj in jsonData["books"]:
        if not jsonObj:
            # 配列が空の場合
            return '積読本なし'
        num += 1
        booklist.append(jsonObj['title'])

    return booklist, num
