import requests
import json
from bs4 import BeautifulSoup
import re

def zaiko(isbn, tenpo_id):
    url = "https://book.yurindo.co.jp/store.asp?isbn=" + isbn + "&tcd=" + tenpo_id
    response = requests.get(url)

    # 文字化け修正
    response.encoding = response.apparent_encoding

    soup = BeautifulSoup(response.text, 'html.parser')

    # 初期値設定
    zaiko_flg = False
    tanainfo = ''

    # '在庫あり'の記載有無を検査
    #  ある場合：在庫フラグ(zaiko_flg)をTrueに設定
    btags = soup.find_all('b')
    for btag in btags:
        if btag.string == '在庫あり':
            # 在庫フラグをTrueに設定
            zaiko_flg = True
            break
    
    # 在庫フラグがTrueの場合、棚番号情報を取得
    if zaiko_flg:
        tdvalues = soup.find_all('td')
        for value in tdvalues:
            if value.string is not None:
                # '棚番号'を含むか否かのフラグ
                tana_flg = '棚番号' in value.string
                if tana_flg:
                    # 棚番号情報を取得
                    match = re.search('(?<=\[).*?(?=\])', value.string)
                    tanainfo = match.group()
                    break

    if zaiko_flg:
        zaiko = '在庫あり'
    else:
        zaiko = '在庫なし'

    return zaiko, tanainfo

def zaiko_all(isbn, tenpo_id_list):
    # zaiko_list:在庫状況リスト（在庫あり、在庫なし）
    zaiko_list = []
    # tanainfo_list:棚番号リスト（棚番号： [X-0-0]）
    tanainfo_list = []
    
    for tenpo_id in tenpo_id_list:
        result = zaiko(isbn,tenpo_id) 
        zaiko_list.append(result[0])
        tanainfo_list.append(result[1])
    return zaiko_list, tanainfo_list

