import requests
import json
import re

def book_quote(isbn):
    url_base = "https://www.googleapis.com/books/v1/volumes?q=isbn:"
    url = url_base + isbn

    response = requests.get(url)
    jsonData = response.json()

    resultlist = {}

    if jsonData["totalItems"] != 0:
        for jsonObj in jsonData["items"]:
            if "title" in jsonObj["volumeInfo"]:
                title = jsonObj["volumeInfo"]["title"]
            else:
                title = "NAN"
            if "authors" in jsonObj["volumeInfo"]:
                authors = jsonObj["volumeInfo"]["authors"]
                authorlist = ','.join(authors)
            else:
                authors = "NAN"
            if "publishedDate" in jsonObj["volumeInfo"]:
                publishedDate = jsonObj["volumeInfo"]["publishedDate"]
            else:
                publishedDate = "NAN"
            if "publisher" in jsonObj["volumeInfo"]:
                publisher = jsonObj["volumeInfo"]["publisher"]
            else:
                publisher = "NAN"
            if "imageLinks" in jsonObj["volumeInfo"]:
                thumbnail = jsonObj["volumeInfo"]["imageLinks"]["thumbnail"]
            else:
                thumbnail = "NAN"
        quote = authorlist + '(' + publishedDate + ')' + '『' + title + '』' + publisher
    else:
        quote = '該当なし'

    resultlist = {"title" : title, "authorlist" : authorlist, "publishedDate" : publishedDate, "publisher" : publisher, "quote" : quote, "thumbnail" : thumbnail}

    return resultlist

def extract_amazoninfo(amazoninfo1, amazoninfo2):
    data1 = amazoninfo1.split("\n")
    data2 = amazoninfo2.split("\n")
    # 初期化
    title = 'NAN'
    authorlist = []
    publishedDate = 'NAN'
    publisher = 'NAN'
    quote = ''

    # データ抽出
    ## 書名
    title = data1[0]
    # 「Kindle版」を除去
    title = re.sub('Kindle版', "", title)
    # 先頭・末尾のスペースを除去
    title = title.strip()

    ## 著者リスト
    authors = re.sub(r'\([^)]*\)', "", data1[1])
    authors = re.sub('形式: Kindle版', "", authors)
    authorslist = authors.split(',')
    for author in authorslist:
        # 先頭・末尾のスペースを除去してリスト追加
        authorlist.append(author.strip())

    for d in data2:
        if '発売日' in d:
            mdate = re.findall(r'\d+', d)
            publishedDate = mdate[0] + '/' + mdate[1] + '/' + mdate[2]
        if '出版社' in d:
            # ':'から'('までの文字列を抽出
            # (1) (xxx)を除去
            mpub = re.sub(r'\([^)]*\)', "", d)
            # (2) ':'以降の文字列を抽出
            mpub = re.findall("(?<=\:).+", mpub) 
            publisher = mpub[0].strip()

    quote = ','.join(authorlist) + '(' + publishedDate + ')' + '『' + title + '』' + publisher
    result = {"title" : title, "authorlist" : ','.join(authorlist), "publishedDate" : publishedDate, "publisher" : publisher, "quote" : quote}
    
    return result
