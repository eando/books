#!/bin/bash

# how to launch

if [ $ENV = "PRO" ]; then
	# PRODUCTION
	echo "========= Production ========="
	waitress-serve --port=8888 --call 'books:create_app'
else
	# DEVELOPMENT
	echo "========= Development ========="
	export FLASK_APP=books
	export FLASK_ENV=development
	flask run
fi
